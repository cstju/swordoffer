package sword_offer_java;
import java.util.ArrayList;

public class problem28 {
	private static ArrayList<String> resultStr = new ArrayList<String>();
    public ArrayList<String> Combination(String str) {
        char[] strs = str.toCharArray();
        if(str.length()==0 || str == null) return resultStr; 
        int strLen = str.length();
        //如果是n个字符，共有2^n-1种组合
        //将1向左移动n步，得打2^n
        int combinationNum = 1<<strLen;
        for(int i=1;i<combinationNum;i++){
        	StringBuffer sb = new StringBuffer();
        	for(int j=0;j<strLen;j++){
        		//对应位上为1，则输出对应的字符
        		if((i&(1<<j))!=0){
        			sb.append(strs[j]);
        		}	
        	}
        	resultStr.add(sb.toString());
        }
        return resultStr;
    }
    public static void main(String[] args) {
    	problem28 test = new problem28();
		test.Combination("abc");
		for (int i=0;i<resultStr.size();i++){
			System.out.println(resultStr.get(i));
		}
		//System.out.print(test.Find(target,array));

	}

}
