package sword_offer_java;

public class problem3 {
	public boolean Find(int target, int [][] array) {
        boolean find = false;
        int arrayColumn = array[0].length;
        int arrayRow = array.length;
        if (array!=null && arrayColumn>0 && arrayRow>0){
            int currentRow = 0;
            int currentColumn = arrayColumn-1;
            while(currentRow<arrayRow && currentColumn>=0){
                if(array[currentRow][currentColumn]==target){
                    find = true;
                    break;
                }
                else if(array[currentRow][currentColumn]>target){
                    currentColumn = currentColumn-1;
                }
                else{
                    currentRow = currentRow+1;
                }
                System.out.println(currentRow);
                System.out.println(currentColumn);
            }
        }
		return find;
    }
	public static void main(String[] args) {
		problem3 test = new problem3();
		int target = 5;
		int[][] array = {{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
		System.out.print(test.Find(target,array));

	}
}
