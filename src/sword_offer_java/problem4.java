package sword_offer_java;

public class problem4 {
    public String replaceSpace(String str) {
        String orginStr = str;
    	int spaceNum = 0;
        for (int i=0;i<orginStr.length();i++){
            if(orginStr.charAt(i)==' '){
                spaceNum += 1;
            }
        }
        char[] newStrArray = new char[orginStr.length()+spaceNum*2];
        int endOfOrginStr = orginStr.length()-1;
        int endOfNewStr = newStrArray.length-1;
        while(endOfOrginStr>=0){
            char currentChar = orginStr.charAt(endOfOrginStr--);
            if (currentChar==' '){
                newStrArray[endOfNewStr--]='0';
                newStrArray[endOfNewStr--]='2';
                newStrArray[endOfNewStr--]='%';
            }else{
                newStrArray[endOfNewStr--]=currentChar;
            }
        }
        return String.valueOf(newStrArray);
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		problem4 test = new problem4();
		String testStr = "  ";
		System.out.println(test.replaceSpace(testStr));

	}

}
