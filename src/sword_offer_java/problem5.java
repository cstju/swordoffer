package sword_offer_java;

import java.util.ArrayList;
import java.util.Stack;
public class problem5 {
    class Node {
    	int val;
    	Node next = null;
    	Node(int val) {
    		this.val = val;
    		}
    	Node(Node node) {
    		this.val = node.val;
    		this.next = node.next;
    		}
    	}
    public Node head;
    public Node current;
	//方法：向链表中添加数据
    public void add(int data) {
        //判断链表为空的时候
        if (head == null) {//如果头结点为空，说明这个链表还没有创建，那就把新的结点赋给头结点
            head = new Node(data);
            current = head;
        } else {
            //创建新的结点，放在当前节点的后面（把新的结点合链表进行关联）
            current.next = new Node(data);
            //把链表的当前索引向后移动一位
            current = current.next;   //此步操作完成之后，current结点指向新添加的那个结点
        }
    }

    //方法：遍历链表（打印输出链表。方法的参数表示从节点node开始进行遍历
    public void print(Node node) {
        if (node == null) {
            return;
        }
        current = node;
        while (current != null) {
            System.out.print(current.val+" ");
            current = current.next;
        }
    }

    ArrayList<Integer> arraylist = new ArrayList<Integer>();
    //从尾到头打印链表
    public ArrayList<Integer> printListFromTailToHead(Node listNode) {
        if (listNode!=null){
            printListFromTailToHead(listNode.next);
            arraylist.add(listNode.val);
        }
        System.out.println(arraylist);
        return arraylist;
    }
    public Node ReverseList(Node head) {
        Stack<Node> nodes = new Stack<Node>();
        //将所有节点都压入到栈中
        Node pNode = head;
        while(pNode!=null){
        	//每次入栈都是新建一个对象不会更改原列表
            nodes.push(new Node(pNode));
            pNode = pNode.next;
        }
        Node newNode = nodes.pop();
        Node newHeadNode = newNode;
        while(!nodes.empty()){
            newNode.next = nodes.pop();
            newNode = newNode.next;
        }
        newNode.next = null;
		return newHeadNode;
    }
    public Node ReverseKList(Node head, int k) {
        Stack<Node> nodes = new Stack<Node>();
        //将所有节点都压入到栈中
        Node pNode = head;
        for(int i=0;i<k;i++){
        	nodes.push(new Node(pNode));
        	pNode = pNode.next;
        }
        Node newNode = nodes.pop();
        Node newHeadNode = newNode;
        while(!nodes.empty()){
            newNode.next = nodes.pop();
            newNode = newNode.next;
        }
        newNode.next = pNode;
		return newHeadNode;
    }
	 public static void main(String[] args) {
		 problem5 list = new problem5();//向LinkList中添加数据
		 for (int i = 0; i < 10; i++) {
			 list.add(i);
			 }
		 list.print(list.head);// 从head节点开始遍历输出
		 System.out.printf("\n反转之后输出\n");
		 Node newNode = list.ReverseList(list.head);
		 list.print(newNode);
		 System.out.printf("\n原列表:\n");
		 list.print(list.head);
		 System.out.printf("\n反转前k个节点输出:\n");
		 Node newNode2 = list.ReverseKList(list.head,5);
		 list.print(newNode2);
		 System.out.printf("\n原列表:\n");
		 list.print(list.head);

		 //list.printListFromTailToHead(list.head);
	 }
}
