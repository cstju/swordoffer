package sword_offer_java;

public class Queen8 {
	//皇后的个数
	private int  queensNum = 16; //累计方案总数  
	//queensCol[i] =j 表示第i列的第j行放置一个皇后，i和j都是从0开始
	private int [] queensCol = new int [queensNum];
	//总可行方案数
	private int existNum = 1;
	//rowExistQueen[i] =true 表示第i行有皇后，i从0开始
	private boolean [] rowExistQueen = new boolean [queensNum];
	//rightHLeftLSlash[i] = true 表示右高左低的第i条斜线有皇后
	private boolean [] rightHLeftLSlash = new boolean[queensNum*2-1];
	//leftHrightLSlash[i] = true 表示左高右低的第i条斜线有皇后
	private boolean [] leftHrightLSlash = new boolean[queensNum*2-1];
	
	//初始化变量
	private void init(){
		for(int i=0;i<queensNum;i++){
			rowExistQueen[i] = false;
		}
		for(int i=0;i<queensNum*2-1;i++){
			rightHLeftLSlash[i]=false;
			leftHrightLSlash[i]=false;
		}
	}
	// 判断该位置是否已经存在一个皇后,存在则返回 true  
	private boolean isExists(int row, int col) {  
        return (rowExistQueen[row]||rightHLeftLSlash[row+col]||leftHrightLSlash[queensNum+row-col-1]);  
    }
	public void testing(int col) {
		// 遍历每一行
		for (int row = 0; row < queensNum; row++) {
			// 如果第 row 行第 col 列可以放置皇后
			if (!isExists(row,col)) {
				// 设置第 row 行第 col 列有皇后   
				queensCol[col] = row;
				// 设置以第 row 行第 col 列为交叉点的斜线不可放置皇后  
				rowExistQueen[row] = true;
				rightHLeftLSlash[row+col] = true;
				leftHrightLSlash[queensNum+row-col-1] = true;  
				
                // 全部尝试过，打印  
                if(col == queensNum-1) { 
                	printChessBoard();
                	existNum ++;
                }else {  
                    // 放置下一列的皇后  
                    testing(col + 1);  
                }  
                // 撤销上一步所放置的皇后，即回溯  
                rowExistQueen[row] = false;
                rightHLeftLSlash[row+col] = false;
                leftHrightLSlash[queensNum+row-col-1] = false;  
            }  
        }  
    }
    public void printChessBoard(){  
    	System.out.println("第"+existNum+"种走法");
    	for(int i=0;i<queensNum;i++){
    		for(int j=0;j<queensNum;j++){
    			if(i==queensCol[j]){
    				System.out.print("0 ");
    				}
    			else System.out.print("+ ");
    			}
    		System.out.println("");
    		}
    	}  
    public static void main(String args[]){
    	Queen8 queen = new Queen8();  
    	queen.init();  
        // 从第0列开始求解  
        queen.testing(0);
    }  
}  

