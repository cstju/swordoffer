package sword_offer_java;

import java.util.Scanner;
import java.util.Stack;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.LinkedList;
import java.util.ArrayList;
public class BinaryTree {
	

    //按照前序遍历的顺序控制台输入用#代表null
	public TreeNode buildTree(char[] nodeList,int index){
		TreeNode node = null;
		TreeNode root = createTree(node,nodeList);
		return root;
	}
    public TreeNode createTree(TreeNode node,char[] nodeList){
    	if(nodeList[id]=='#') {
    		id++;
    		return null;
    	}else{
    		node = new TreeNode(nodeList[id++]);
    		//递归构建二叉树
    		node.setLeft(createTree(node.getLeft(),nodeList));
    		node.setRight(createTree(node.getRight(),nodeList));
    	}
		return node;
    }
    // 先序遍历（递归） —— 根、左、右  
    public void preOrderTraverse(TreeNode root) {  
        if (root != null) {
            System.out.print(root.getVal() + " ");  
            preOrderTraverse(root.getLeft());  
            preOrderTraverse(root.getRight()); 
        }  
    }
    // 先序遍历（非递归）  
    public void nrPreOrderTraverse(TreeNode root) {  
        Stack<TreeNode> stack = new Stack<TreeNode>();  
        TreeNode node = root;  
        while (node != null || !stack.isEmpty()) { // 右子树不为空，或者栈不为空  
            while (node != null) {  
            	// 遍历根节点和左子树的一系列的“根”，并将它们全部入栈 
                System.out.print(node.getVal() + " ");  
                stack.push(node);  
                node = node.getLeft();  
            }
            if(!stack.isEmpty()){
            	// 弹出此时最里面的左子树根节点，判断其右子树情况
            	node = stack.pop();  
            	// 处理右子树情况  
            	node = node.getRight(); // 处理右子树情况 
            }
        }  
    }
	// 中序遍历（递归） —— 左、根、右  
    public void inOrderTraverse(TreeNode root) {  
        if (root != null) {
        	inOrderTraverse(root.getLeft());  
            System.out.print(root.getVal() + " ");  
            inOrderTraverse(root.getRight()); 
        }  
    }
    // 中序遍历（非递归）  
    public void nrInOrderTraverse(TreeNode root) {  
        Stack<TreeNode> stack = new Stack<TreeNode>();  
        TreeNode node = root;  
        while (node != null || !stack.isEmpty()) { // 右子树不为空，或者栈不为空  
            while (node != null) {  
            	//从根或右子树开始，把所有的左孩子的左孩子全部入栈，直到找到最里面的左孩子 
                stack.push(node);  
                node = node.getLeft();  
            }
            if(!stack.isEmpty()){
            	// 出栈左孩子或者根节点
            	node = stack.pop();
            	System.out.print(node.getVal() + " ");
            	// 处理右子树情况  
            	node = node.getRight(); // 处理右子树情况  
            }
        }  
    }
	// 后序遍历（递归） —— 左、右、根
    public void postOrderTraverse(TreeNode root) {  
        if (root != null) {
        	postOrderTraverse(root.getLeft());  
        	postOrderTraverse(root.getRight()); 
            System.out.print(root.getVal() + " ");  
        }  
    }
    // 后序遍历（非递归）  
    public void nrPostOrderTraverse(TreeNode root) {  
        Stack<TreeNode> stack = new Stack<TreeNode>();  
        TreeNode node = root; 
        TreeNode currentNode = null;//最后一次访问的节点
        // 右子树不为空，或者栈不为空，子树已经处理过
        while (node != null || !stack.isEmpty()) {   
            while (node != null) {  
            	//从根或右子树开始，把所有的左孩子的左孩子全部入栈，直到找到最里面的左孩子 
                stack.push(node);  
                node = node.getLeft();  
            }
            //查看栈顶元素,但不移除它
            node = stack.peek();
            // 该节点的右子树为空，或者其右子树已经被访问过 
            if (node.getRight()==null || node.getRight()==currentNode) {  
                System.out.print(node.getVal() + " "); // 访问该节点  
                node = stack.pop();  
                currentNode = node;  
                node = null; // 将node置为空，用于判断栈中的下一元素  
            } else {  
                node = node.getRight();  
            } 
        }  
    }
    //层次遍历 采用队列，先进先出
    public void levelTraverse(TreeNode root) {  
    	Queue<TreeNode> queue = new LinkedList<TreeNode>(); // Queue为接口  
        queue.add(root);  
        while (!queue.isEmpty()) {  
            TreeNode temp = queue.poll(); // queue.poll()获取并移除队头元素 
            if (temp != null) {  
                System.out.print(temp.getVal() + " ");  
                if (temp.getLeft() != null) {  
                    queue.add(temp.getLeft()); 
                }  
                if (temp.getRight() != null) {  
                    queue.add(temp.getRight()); 
                }  
            }  
        }  
    }
    //层次遍历 要求按层输出 采用两个队列
    //将下面程序中的换行输出的注释去掉就可以换行输出层次遍历树
    public void levelTraverse2(TreeNode root){
    	System.out.print(root.getVal()+ " ");
    	System.out.println();
    	ArrayList<TreeNode> former = new ArrayList<TreeNode>();
        former.add(root);
        while (former.size() != 0) {
        	ArrayList<TreeNode> current = new ArrayList<TreeNode>();
            for (TreeNode node : former) {
            	if (node.getLeft() != null) {
            		System.out.print(node.getLeft().getVal() + " ");
            		current.add(node.getLeft());
                }
                if (node.getRight() != null) {
                	System.out.print(node.getRight().getVal() + " ");
                	current.add(node.getRight());
                }
            }
            System.out.println();
            former.clear();
            former.addAll(current);
        }
    }
    //使用全局变量id来确定节点值，还没有想到更好的方法
    public static int id = 0;
    public static void main(String[] args) {  
        //int[] a = {2,4,12,45,21,6,111};  
//    	Scanner scan = new Scanner(System.in);
//    	System.out.println("二叉树节点输入，null用#表示：");
//    	String inputStr = scan.nextLine();
//    	scan.close();
    	String inputStr = "ABC#D##EF###GH##IJ##K##";
    	/*
    	 *                  A
    	 *          B               G
    	 *      C       E       H        I
    	 *    #   D   F   #   #   #    J    K
    	 *       # # # #              # #  # #
    	 */    
    	char[] inputCharList = inputStr.toCharArray();
    	//final static int id = 0;
    	
    	BinaryTree tree = new BinaryTree();
    	TreeNode root = tree.buildTree(inputCharList,id);
    	/*
    	System.out.print("前序结果递归输出：");
        tree.preOrderTraverse(root);
        System.out.print("\n前序结果非递归输出：");
        tree.nrPreOrderTraverse(root);
        System.out.print("\n中序结果递归输出：");
        tree.inOrderTraverse(root);
        System.out.print("\n中序结果非递归输出：");
        tree.nrInOrderTraverse(root);
        System.out.print("\n后序结果递归输出：");
        tree.postOrderTraverse(root);
        System.out.print("\n后序结果非递归输出：");
        tree.nrPostOrderTraverse(root);
        */
        System.out.print("\n层次遍历输出1：");
        tree.levelTraverse(root);
        System.out.print("\n层次遍历输出2：");
        tree.levelTraverse2(root);

    } 

}
