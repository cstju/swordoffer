package sword_offer_java;

public class TreeNode {  
	private TreeNode left = null;  
	private TreeNode right = null;  
	private char val = '#';  
	
	public TreeNode(){  
	          
	    }  
	public TreeNode(char val) {
		this.val = val;  
	}  
	public TreeNode(TreeNode left, TreeNode right, char val) {
		this.left = left;  
	    this.right = right;  
	    this.val = val;
	}
	public TreeNode getLeft() {
		return left;
	}  
	public void setLeft(TreeNode left) {  
		this.left = left;  
	}  
	public TreeNode getRight() {  
	    return right;  
	}  
	public void setRight(TreeNode right) {  
	    this.right = right;  
	}  
	public char getVal() {  
	    return val;  
	}  
	public void setVal(char val) {  
	    this.val = val;  
	}  
}  
