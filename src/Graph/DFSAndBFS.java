package Graph;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import Graph.ListUDG;
import Graph.ListDG;
import Graph.MatrixDG;
import Graph.MatrixUDG;
import Graph.VertexNode;
import Graph.EdgeNode;
public class DFSAndBFS {
    /*
     * 邻接表深度优先搜索遍历图(递归)
     * 时间复杂度O(顶点数+边数)
     */
	private void subDFSList(VertexNode[] vertexArray,int i, boolean[] visited) {
        EdgeNode currentNode;
        visited[i] = true;
        System.out.printf("%c ", vertexArray[i].nodeData);
        //获取当前顶点的第一个指向的顶点
        currentNode = vertexArray[i].firstEdge;
        while (currentNode != null) {
        	//如果该顶点没有被遍历过则遍历该顶点
            if (!visited[currentNode.indexVertex])
            	subDFSList(vertexArray,currentNode.indexVertex, visited);
            currentNode = currentNode.nextEdge;
        }
    }


    public void DFSList(VertexNode[] vertexArray) {
        boolean[] visited = new boolean[vertexArray.length];       // 顶点访问标记

        // 初始化所有顶点都没有被访问
        for (int i = 0; i <vertexArray.length; i++)
            visited[i] = false;

        System.out.printf("== DFS: ");
        for (int i = 0; i < vertexArray.length; i++) {
            if (!visited[i]) subDFSList(vertexArray,i, visited);
        }
        System.out.printf("\n");
    }
    /*
     * 邻接矩阵深度优先搜索遍历图(递归)
     * 时间复杂度O(顶点数^2)
     */
	private void subDFSMatrix(char[] vertexArray,int[][] edgeMatrix,int i, boolean[] visited) {
        visited[i] = true;
        System.out.printf("%c ", vertexArray[i]);
        for (int j = 0; j < vertexArray.length; j++) {
            if (edgeMatrix[i][j]==1 && !visited[j])
            	subDFSMatrix(vertexArray,edgeMatrix,j, visited);
        }
    }

    public void DFSMatrix(char[] vertexArray,int[][] edgeMatrix) {
        boolean[] visited = new boolean[vertexArray.length];       // 顶点访问标记

        // 初始化所有顶点都没有被访问
        for (int i = 0; i <vertexArray.length; i++)
            visited[i] = false;

        System.out.printf("== DFS: ");
        for (int i = 0; i < vertexArray.length; i++) {
            if (!visited[i])
            	subDFSMatrix(vertexArray,edgeMatrix,i, visited);
        }
        System.out.printf("\n");
    }
    /*
     * 邻接表广度优先遍历
     */
    public void BFSList(VertexNode[] vertexArray){
    	EdgeNode currentNode;
    	VertexNode currentVertex;
    	//新建一个队列，用于存储当前顶点
    	Queue<VertexNode> queue = new LinkedBlockingQueue<VertexNode>();
    	boolean[] visited = new boolean[vertexArray.length];
        // 初始化所有顶点都没有被访问
        for (int i = 0; i <vertexArray.length; i++){
            visited[i] = false;
        }
        System.out.printf("== BFS: ");
        for (int i=0; i< vertexArray.length;i++){
        	if(!visited[i]){
        		visited[i] = true;
        		System.out.printf("%c ", vertexArray[i].nodeData);
        		queue.add(vertexArray[i]);
        		while(!queue.isEmpty()){
        			currentVertex = queue.poll();
        			//找到当前顶点指向的第一个边表指针
        			currentNode = currentVertex.firstEdge;
        			while(currentNode != null){
        				//如果当前顶点没有被访问过
        				if(!visited[currentNode.indexVertex]){
        					visited[currentNode.indexVertex]=true;
        					//输出此边表在顶点表中对应信息
        					System.out.printf("%c ", vertexArray[currentNode.indexVertex].nodeData);
        					queue.add(vertexArray[currentNode.indexVertex]);
        				}
        				currentNode = currentNode.nextEdge;
        			}
        		}
        	}
        }
        System.out.printf("\n");
    	
    }
    /*
     * 邻接矩阵广度优先遍历
     */
    public void BFSMatrix(char[] vertexArray,int[][] edgeMatrix) {
    	EdgeNode currentNode;
    	VertexNode currentVertex;
    	int vertexIndex;
    	int currentIndex;
    	//新建一个队列，用于存储当前顶点
    	Queue<Integer> queue = new LinkedBlockingQueue<Integer>();
    	boolean[] visited = new boolean[vertexArray.length];
        // 初始化所有顶点都没有被访问
        for (int i = 0; i <vertexArray.length; i++){
            visited[i] = false;
        }
        System.out.printf("== BFS: ");
        for (int i=0; i< vertexArray.length;i++){
        	if(!visited[i]){
        		visited[i] = true;
        		System.out.printf("%c ", vertexArray[i]);
        		//将顶点下标放到队列中
        		queue.add(i);
        		while(!queue.isEmpty()){
        			vertexIndex = queue.poll();
        			//找到当前顶点指向的第一个边表指针
        			//查找与当前顶点连接的其他顶点
        			for(currentIndex = 0;currentIndex<vertexArray.length;currentIndex++){
        				if(edgeMatrix[vertexIndex][currentIndex]==1 && !visited[currentIndex]){
        					//访问其他顶点并将其放入队列中
        					visited[currentIndex] = true;
        					System.out.printf("%c ", vertexArray[currentIndex]);
        					queue.add(currentIndex);		
        				}
        			}
        		}
        	}
        }
        System.out.printf("\n");
    }
	
	
	
	public static void main(String[] args){
//		char[] vertexs = {'A','B','C','D','E','F','G'};
//		char[][] edges = new char[][] {{'A','C'},{'A','D'},{'A','F'},{'B','C'},
//										{'C','D'},{'E','G'},{'F','G'}};
		char[] vertexs = {'A','B','C','D','E','F','G','H','I'};
		char[][] edges = new char[][] {{'A','B'},{'A','F'},{'B','C'},
										{'B','I'},{'B','G'},{'F','G'},
										{'F','E'},{'C','I'},{'C','D'},
										{'I','D'},{'G','D'},{'G','H'},
										{'E','D'},{'E','H'},{'H','D'}};
		DFSAndBFS dB = new DFSAndBFS();
		
		ListDG dG;
		dG = new ListDG(vertexs, edges);
		//pG = new ListDG();
		dG.print();
		
		System.out.printf("邻接表深度优先遍历有向图:\n");
		dB.DFSList(dG.vertexNodeArray);
		System.out.printf("\n邻接表广度优先遍历有向图:\n");
		dB.BFSList(dG.vertexNodeArray);
		
		System.out.printf("\n=======================\n");
		ListUDG uDG;
		uDG = new ListUDG(vertexs, edges);
		//pG = new ListUDG();
		uDG.print();
		System.out.printf("\n邻接表深度优先遍历无向图:\n");
		dB.DFSList(uDG.vertexNodeArray);
		System.out.printf("\n邻接表广度优先遍历无向图:\n");
		dB.BFSList(uDG.vertexNodeArray);
		
		System.out.printf("\n=======================\n");
		MatrixDG mDG;
		mDG = new MatrixDG(vertexs, edges);
		//mDG = new MatrixDG();
		mDG.print();
		System.out.printf("\n邻接矩阵深度优先遍历有向图:\n");
		dB.DFSMatrix(mDG.vertexArray,mDG.edgeMatrix);
		System.out.printf("\n邻接矩阵广度优先遍历有向图:\n");
		dB.BFSMatrix(mDG.vertexArray,mDG.edgeMatrix);
		
		System.out.printf("\n=======================\n");
		MatrixUDG mUDG;
		mUDG = new MatrixUDG(vertexs, edges);
		//mUDG = new MatrixUDG();
		mUDG.print();
		System.out.printf("\n邻接矩阵深度优先遍历无向图:\n");
		dB.DFSMatrix(mUDG.vertexArray,mUDG.edgeMatrix);
		System.out.printf("\n邻接矩阵广度优先遍历无向图:\n");
		dB.BFSMatrix(mUDG.vertexArray,mUDG.edgeMatrix);
		
		
		
	}

}
