package Graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import Graph.ListUDG;
import Graph.ListDG;
import Graph.MatrixDG;
import Graph.MatrixUDG;
import Graph.VertexNode;
import Graph.EdgeNode;
/**
 * 拓扑排序，判断是否有环
 * @author 3dlabuser


 */
public class TopologicalSort {
	
	/* 返回值：
	 * -1 -- 失败(由于内存不足等原因导致)
	 * 0 -- 成功排序，并输入结果
	 * 1 -- 失败(该有向图是有环的)
	 */
	public char[] sort(VertexNode[] vertexArray){
		int index = 0;
		int vertexNum = vertexArray.length;
		//记录入度数组
		int[] inNum = new int[vertexNum];
		//记录拓扑排序结果
		char[] sortResult = new char[vertexNum];
		Queue<Integer> queue = new LinkedBlockingQueue<Integer>();
		EdgeNode currentNode;
		//统计每个顶点的入度
		for(int i=0;i<vertexNum;i++){
			currentNode = vertexArray[i].firstEdge;
			while(currentNode!=null){
				inNum[currentNode.indexVertex]++;
				currentNode = currentNode.nextEdge;
			}
		}
		//将所有入度为0的顶点下标放入队列中
		for(int i=0;i<vertexNum;i++){
			if(inNum[i]==0) queue.offer(i);
		}
		while(!queue.isEmpty()){
			int currentVertexIndex = queue.poll();
			//将当前顶点字符放入结果数组中
			sortResult[index++] = vertexArray[currentVertexIndex].nodeData;
			//获取以该顶点为起点的出边队列
			currentNode = vertexArray[currentVertexIndex].firstEdge;
			//将与currentNode对应的顶点的入度减1
			//若减1之后该顶点的入度为0，则将该顶点添加到队列中
			while(currentNode!=null){
				//将下标为currentNode.indexVertex的顶点的入度减一
				inNum[currentNode.indexVertex]--;
				//如果顶点的入度为0则将其放入队列
				if(inNum[currentNode.indexVertex]==0){
					queue.offer(currentNode.indexVertex);
				}
				currentNode = currentNode.nextEdge;
			}
		}
		if(index!=vertexNum){
			System.out.printf("此有向图有环\n");
		}
		return sortResult;
	}
	
	public static void main(String[] args){
		char[] vertexs = {'A','B','C','D','E','F','G'};
		char[][] edges = new char[][] {{'A','C'},{'A','D'},{'A','F'},{'B','C'},
										{'C','D'},{'E','G'},{'F','G'}};
//		char[] vertexs = {'A','B','C','D','E','F','G','H','I'};
//		char[][] edges = new char[][] {{'A','B'},{'A','F'},{'B','C'},
//										{'B','I'},{'B','G'},{'F','G'},
//										{'F','E'},{'C','I'},{'C','D'},
//										{'I','D'},{'G','D'},{'G','H'},
//										{'E','D'},{'E','H'},{'H','D'}};
		TopologicalSort dB = new TopologicalSort();
		
		ListDG dG;
		dG = new ListDG(vertexs, edges);
		//pG = new ListDG();
		dG.print();
		
		char[] sortArray = dB.sort(dG.vertexNodeArray);
		System.out.printf("== TopSort: ");
		for(int i = 0; i < sortArray.length; i ++)
			System.out.printf("%c ", sortArray[i]);
		System.out.printf("\n");
		
	}

}
