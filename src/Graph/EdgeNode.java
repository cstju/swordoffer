package Graph;

public class EdgeNode{
	//该边所指向的顶点在顶点数组中的下标位置
	int indexVertex;
	//该弧的权值
	int edgeWeight=1;
	//指向下一条弧的指针
	EdgeNode nextEdge;
}
