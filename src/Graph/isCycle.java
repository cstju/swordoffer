package Graph;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import Graph.ListUDG;
import Graph.ListDG;
import Graph.MatrixDG;
import Graph.MatrixUDG;
import Graph.VertexNode;
import Graph.EdgeNode;
/**
 * 采用深度优先遍历方法判断一个有向图有没有环
 * @author 3dlabuser
 *
 */
public class isCycle {
	//在方法外建立一个标志
	boolean flag = false;
	
	private void subIsCycleGraph(VertexNode[] vertexArray,int i, int[] visited) {
		EdgeNode currentNode;
        visited[i] = 0;
        //获取当前顶点的第一个指向的顶点
        currentNode = vertexArray[i].firstEdge;
        while (currentNode != null) {
        	//如果该顶点没有被遍历过则遍历该顶点
            if (visited[currentNode.indexVertex]==-1)
            	subIsCycleGraph(vertexArray,currentNode.indexVertex, visited);
            else if(visited[currentNode.indexVertex]==0){
            	//如果有环就将标志设为true并退出
            	flag = true;
            	return;
            }
            currentNode = currentNode.nextEdge;
        }
        visited[i] = 1;
    }
	
	public boolean isCycleGraph(VertexNode[] vertexArray){
	    // 顶点访问标记 -1:未访问 0:正在访问 1:已访问
		int[] visited = new int[vertexArray.length];

	    // 初始化所有顶点都没有被访问
		for (int i = 0; i <vertexArray.length; i++)
			visited[i] = -1;
	    for (int i = 0; i < vertexArray.length; i++) {
	    	if (visited[i]==-1){
	    		subIsCycleGraph(vertexArray,i, visited);
	    		//如果有环则直接终止函数并返回标志
	    		if(flag) {
	    			return flag;
	    		}
	    	}
	    }
		return flag;
	}
	
	public static void main(String[] args){
		char[] vertexs = {'A','B','C','D','E','F','G'};
		char[][] edges = new char[][] {{'A','C'},{'D','A'},{'A','F'},{'B','C'},
										{'C','D'},{'E','G'},{'F','G'}};
//		char[] vertexs = {'A','B','C','D','E','F','G','H','I'};
//		char[][] edges = new char[][] {{'A','B'},{'A','F'},{'B','C'},
//										{'B','I'},{'B','G'},{'F','G'},
//										{'F','E'},{'C','I'},{'C','D'},
//										{'I','D'},{'G','D'},{'G','H'},
//										{'E','D'},{'E','H'},{'H','D'}};
		isCycle dB = new isCycle();
		boolean flag;
		
		ListDG dG;
		dG = new ListDG(vertexs, edges);
		//pG = new ListDG();
		dG.print();
		
		System.out.printf("邻接表有向图深度优先遍历是否有环:\n");
		flag = dB.isCycleGraph(dG.vertexNodeArray);
		if(flag) System.out.printf("此有向图有环\n");
		else System.out.printf("此有向图无环\n");
	}
}

