package Graph;
/**
 * 邻接矩阵建立无向图
 * 构建的邻接矩阵默认无权值
 * @author 3dlabuser
 *
 */
import java.util.Scanner;


public class MatrixUDG {
	public char[] vertexArray;
	public int[][] edgeMatrix;
	
	public MatrixUDG(char[] vertexs,char[][] edges){
		//初始化顶点数,弧数
		int vertexNum = vertexs.length;
		int edgeNum = edges.length;
		//初始化顶点
		vertexArray = new char[vertexNum];
		for(int i=0;i<vertexNum;i++){
			vertexArray[i] = vertexs[i];
		}
		//初始化弧
		edgeMatrix = new int[vertexNum][vertexNum];
		for(int i=0;i<edgeNum;i++){
			int position1 = getPosition(edges[i][0]);
			int position2 = getPosition(edges[i][1]);
			//在矩阵中将对应位置设为1
			edgeMatrix[position1][position2] = 1;
			edgeMatrix[position2][position1] = 1;
		}
	}
	public MatrixUDG(){
		Scanner scanner = new Scanner(System.in);
		System.out.printf("input vertex number: ");
		
		int vertexNum = scanner.nextInt();
		System.out.printf("input edge number: ");
		int edgeNum = scanner.nextInt();
		if(vertexNum<1||edgeNum<1 || (edgeNum>(vertexNum*(vertexNum-1)))){
			System.out.printf("input error: invalid parameters!\n");
            return;
		}
		//初始化顶点
		vertexArray = new char[vertexNum];
		System.out.printf("input the vertex: ");
		for(int i=0;i<vertexNum;i++){
			vertexArray[i] = scanner.next().trim().charAt(0);
		}
		//初始化弧
		edgeMatrix =  new int[vertexNum][vertexNum];
		for(int i=0;i<edgeNum;i++){
			System.out.printf("the %d edge: ",i);
			char c1 = scanner.next().trim().charAt(0);
			char c2 = scanner.next().trim().charAt(0);
			//读取弧的起始点和结束点在顶点数组中的位置
			int position1 = getPosition(c1);
			int position2 = getPosition(c2);
			//在矩阵中将对应位置设为1
			edgeMatrix[position1][position2] = 1;
			edgeMatrix[position2][position1] = 1;
		}	
		scanner.close();
	}
	private int getPosition(char nodeData){
		for(int i=0;i<vertexArray.length;i++){
			if(vertexArray[i]==nodeData) return i;
		}
		return -1;
	} 
	public void print(){
		System.out.println("Matrix Graph:\n");
		for(int i=0;i<vertexArray.length;i++){
			for(int j=0;j<vertexArray.length;j++){
				System.out.printf("%d ",edgeMatrix[i][j]);
			}
			System.out.printf("\n");
		}
	}
	public static void main(String[] args){
		char[] vertexs = {'A','B','C','D','E','F','G'};
		char[][] edges = new char[][] {{'A','C'},{'A','D'},{'A','F'},{'B','C'},
										{'C','D'},{'E','G'},{'F','G'}};
		MatrixUDG pG;
		//pG = new MatrixUDG(vertexs, edges);
		pG = new MatrixUDG();
		pG.print();
	}

}
