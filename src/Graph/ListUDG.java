package Graph;
/**
 * 邻接表建立无向图
 * 构建的邻接表默认是无权值的
 * @author 3dlabuser
 *
 */
import java.util.Scanner;
import Graph.VertexNode;
import Graph.EdgeNode;

public class ListUDG {
	//顶点数组
	public VertexNode[] vertexNodeArray;
	
	//根据输入顶点和弧构建邻接表
	public ListUDG(char[] vertexs,char[][] edges){
		//初始化顶点数和边数
		int vertexsNodeNum = vertexs.length;
		int edgeNum = edges.length;
		//初始化顶点
		vertexNodeArray = new VertexNode[vertexsNodeNum];
		for(int i=0;i<vertexsNodeNum;i++){
			vertexNodeArray[i] = new VertexNode();
			vertexNodeArray[i].nodeData = vertexs[i];
			vertexNodeArray[i].firstEdge = null;
		}
		//初始化边
		for(int i=0;i<edgeNum;i++){
			//读取弧的起始点和结束点在顶点数组中的位置
			int position1 = getPosition(edges[i][0]);
			int position2 = getPosition(edges[i][1]);
			//初始化node1 node2
			EdgeNode node1 = new EdgeNode();
			node1.indexVertex = position1;
			EdgeNode node2 = new EdgeNode();
			node2.indexVertex = position2;
			//将node2连接到position1所在链表的末尾
			if(vertexNodeArray[position1].firstEdge==null){
				vertexNodeArray[position1].firstEdge = node2;
			}else{
				linkLast(vertexNodeArray[position1].firstEdge,node2);
			}
			//将node1连接到position2所在链表的末尾
			if(vertexNodeArray[position2].firstEdge==null){
				vertexNodeArray[position2].firstEdge = node1;
			}else{
				linkLast(vertexNodeArray[position2].firstEdge,node1);
			}
		}
	}

	//根据控制台输入建立邻接表
	public ListUDG(){
		System.out.printf("input vertex number: ");
		Scanner scanner = new Scanner(System.in);
		int vertexNum = scanner.nextInt();
		System.out.printf("input edge number: ");
		int edgeNum = scanner.nextInt();
		if(vertexNum<1||edgeNum<1 || (edgeNum>(vertexNum*(vertexNum-1)))){
			System.out.printf("input error: invalid parameters!\n");
            return;
		}
		//初始化顶点
		vertexNodeArray = new VertexNode[vertexNum];
		System.out.printf("input the vertex: ");
		for(int i=0;i<vertexNum;i++){
			vertexNodeArray[i] = new VertexNode();
			vertexNodeArray[i].nodeData = scanner.next().trim().charAt(0);
			vertexNodeArray[i].firstEdge = null;
		}
		//初始化弧
		for(int i=0;i<edgeNum;i++){
			System.out.printf("the %d edge: ",i);
			char c1 = scanner.next().trim().charAt(0);
			char c2 = scanner.next().trim().charAt(0);
			//读取弧的起始点和结束点在顶点数组中的位置
			int position1 = getPosition(c1);
			int position2 = getPosition(c2);
			//初始化node1 node2
			EdgeNode node1 = new EdgeNode();
			node1.indexVertex = position1;
			EdgeNode node2 = new EdgeNode();
			node2.indexVertex = position2;
			//将node2连接到position1所在链表的末尾
			if(vertexNodeArray[position1].firstEdge==null){
				vertexNodeArray[position1].firstEdge = node2;
			}else{
				linkLast(vertexNodeArray[position1].firstEdge,node2);
			}
			//将node1连接到position2所在链表的末尾
			if(vertexNodeArray[position2].firstEdge==null){
				vertexNodeArray[position2].firstEdge = node1;
			}else{
				linkLast(vertexNodeArray[position2].firstEdge,node1);
			}
		}	
		scanner.close();
	}
	//返回nodeData在顶点数组中的下标位置
	private int getPosition(char nodeData){
		for(int i=0;i<vertexNodeArray.length;i++){
			if(vertexNodeArray[i].nodeData==nodeData) return i;
		}
		return -1;
	} 
	//将node顶点连接到边表的最后位置
	private void linkLast(EdgeNode list,EdgeNode node){
		//声明一个当前节点
		EdgeNode currentNode = list;
		while(currentNode.nextEdge!=null){
			currentNode = currentNode.nextEdge;
		}
		currentNode.nextEdge=node;
	}
	//打印矩阵图
	public void print(){
		System.out.println("List Graph:");
		for(int i=0;i<vertexNodeArray.length;i++){
			System.out.printf("%d(%c): ", i,vertexNodeArray[i].nodeData);
			EdgeNode node = vertexNodeArray[i].firstEdge;
			while(node!=null){
				System.out.printf("%d(%c): ",
						node.indexVertex,vertexNodeArray[node.indexVertex].nodeData);
				node = node.nextEdge;
			}
			System.out.printf("\n");
		}
	}
	public static void main(String[] args){
		char[] vertexs = {'A','B','C','D','E','F','G'};
		char[][] edges = new char[][] {{'A','C'},{'A','D'},{'A','F'},{'B','C'},
										{'C','D'},{'E','G'},{'F','G'}};
		ListUDG pG;
		pG = new ListUDG(vertexs, edges);
		//pG = new ListUDG();
		pG.print();
	}
}
